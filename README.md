This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

To be able to run this project it is required have installed node and npm.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `After running the app you are able to see the app as image show up following:`

![picture](src/assets/almundo.gif)
