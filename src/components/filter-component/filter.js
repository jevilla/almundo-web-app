import React, { Component } from 'react';
import { strings, constants, range } from '../../utils';
import { Icon } from '../icon-component/icon';
import _ from 'lodash';
import './filter-style.css';

class Filter extends Component {
    initialState = {
        filterHotelNameShown: false,
        filterStarsShown: false,
        starsFilter: { 5: false, 4: false, 3: false, 2: false, 1: false },
        allStars: false,
        starsQuery: []
    }

    constructor(props) {
        super(props);
        this.searchRef = React.createRef();
        this.state = {
            ...this.initialState
        }
    }

    searchByHotelName = (event) => {
        event.preventDefault();
        const hotelName = this.searchRef.current.value;

        if (!_.isEmpty(hotelName.trim())) {
            this.props.getHotelByName(hotelName);
            this.setState({ ...this.initialState })
        } else {
            this.props.getAllHotels();
        }
    }

    renderStars = (numberOfStars) => {
        return range(1, numberOfStars).map(number => {
            return <Icon key={number} type={constants.icons.STAR} />
        });
    }

    setAllStars = (event) => {
        const value = event.target.checked
        this.setState({ allStars: value, starsFilter: this.initialState.starsFilter });
        if (value) {
            this.props.getAllHotels();
        }
    }

    starsFilterSection = () => {
        const {starsFilter} = this.state;

        return Object.keys(starsFilter).reverse().map((key, index) => {
            return (
                <div key={key}>
                    <input
                        type="checkbox"
                        checked={starsFilter[key]}
                        onChange={(event) => this.checkValueChange(event, key)}
                    />
                    {this.renderStars(key)}
                </div>
            );
        });
    }

    checkValueChange = (event, key) => {
        const value = event.target.checked

        const starObject = Object.assign({}, this.state.starsFilter);
        starObject[key] = value;
        this.setState({ starsFilter: starObject, allStars: false });
        this.fillStarsQuery(starObject);
    }

    fillStarsQuery = (object) => {
        const starsQueryTemp = [];
        Object.keys(object).map(key => {
            if (object[key]) starsQueryTemp.push({ stars: +key });
        });
        if (!_.isEmpty(starsQueryTemp)) {
            this.props.getHotelsByStars(starsQueryTemp);
        }
    }

    showOrHideHotelNameFilter = () => {
        this.setState({ filterHotelNameShown: !this.state.filterHotelNameShown })
    }

    showOrHideStarsFilter = () => {
        this.setState({ filterStarsShown: !this.state.filterStarsShown })
    }

    renderUpDown = (show) => {
        return show ? <span className="hotel-name-title" id="updown">▲</span> : 
                    <span className="hotel-name-title" id="updown">▼</span>;
    }
    
    render() {
        return (
            <div id="filter">
                <div id="header">
                    <span id="filter-title">{strings.FILTER}</span>
                </div>
                <div id="hotel-name-filter">
                    <div onClick={this.showOrHideHotelNameFilter} id="name-filter-header">
                        <Icon type={constants.icons.SEARCH} />
                        <span className="hotel-name-title">{strings.HOTEL_NAME}</span>
                        {this.renderUpDown(this.state.filterHotelNameShown)}
                    </div>
                    {this.state.filterHotelNameShown &&
                        <div id="hotel-name-search">
                            <input 
                                ref={this.searchRef} 
                                id="search-input" 
                                type="text" 
                                placeholder={strings.PLACEHOLDER_FILTER_HOTEL} 
                            />
                            <button onClick={this.searchByHotelName} id="action-button">{strings.ACCEPT_BUTTON_TEXT}</button>
                        </div>
                    }
                </div>
                <div id="hotel-name-filter">
                    <div onClick={this.showOrHideStarsFilter} id="name-filter-header">
                        <Icon type={constants.icons.STAR} />
                        <span className="hotel-name-title">{strings.FILTER_BY_STARS}</span>
                        {this.renderUpDown(this.state.filterStarsShown)}
                    </div>
                    {this.state.filterStarsShown &&
                        <div>
                            <input 
                                type="checkbox"
                                checked={this.state.allStars}
                                onChange={this.setAllStars}
                            />
                            <span id="all-stars">{strings.ALL_STARS}</span>
                            {this.starsFilterSection()}
                        </div>
                    }
                </div>
            </div>
        );
    }
}

export { Filter };
