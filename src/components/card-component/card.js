import React, { Component } from 'react';
import { strings, range, constants } from '../../utils';
import { Icon } from '../icon-component/icon';
import './card-style.css';

class Card extends Component {

    hotelStars = (stars) => range(1, stars).map(n => <span key={n} className="icon"><Icon type={constants.icons.STAR} /></span>);

    hotelAmenities = (amenities) => {
        return amenities.map((amenity, i) => {
            return (
                <span key={i} className="icon">
                    <Icon type={amenity} />
                </span>
            );
        });
    }

    render() {
        const {amenities, image, name, price, stars} = this.props;
        const imgUrl = strings.ROOT_URL_IMAGES.concat(image);

        return (
            <div id="card">
                <div id="hotel-general-info">
                    <img id="hotel-image" src={imgUrl} alt="hotel" />
                    <div id="hotel-info">
                        <span id="hotel-name">{name}</span>
                        <span className="icons-container">
                            {this.hotelStars(stars)}
                        </span>
                        <span className="icons-container">
                            {this.hotelAmenities(amenities)}
                        </span>
                    </div>
                </div>
                <div id="hotel-price-info">
                    <div id="text-container">
                        <span id="price-text-info">{strings.PRICE_PER_BEDROOM_TEXT}</span>
                    </div>
                    <div id="currency-container">
                        <span className="price currency ">{strings.CURRENCY_ARS}</span>
                        <span className="price amount">{price}</span>
                    </div>
                    <button id="check-hotel-button">{strings.CHECK_HOTEL}</button>
                </div>
            </div>
        );
    }
}

export { Card };
