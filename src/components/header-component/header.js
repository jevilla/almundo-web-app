import React, { Component } from 'react';
import './header-style.css';
import { Icon } from '../icon-component/icon';

class Header extends Component {
    render() {
        return (
            <header>
                <Icon type="almundo" />
            </header>
        );
    }
}

export { Header };
