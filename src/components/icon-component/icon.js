import React, { Component } from 'react';
import { constants } from '../../utils';
import star from '../../assets/icons/star.svg';
import bathrobes from '../../assets/icons/bathrobes.svg';
import bathtub from '../../assets/icons/bathtub.svg';
import beachPoolFacilities from '../../assets/icons/beach-pool-facilities.svg';
import beach from '../../assets/icons/beach.svg';
import businesCenter from '../../assets/icons/business-center.svg';
import childrenClub from '../../assets/icons/children-club.svg';
import coffeMaker from '../../assets/icons/coffe-maker.svg';
import deepSoakingBathtub from '../../assets/icons/deep-soaking-bathtub.svg';
import fitnessCenter from '../../assets/icons/fitness-center.svg';
import garden from '../../assets/icons/garden.svg';
import kitchenFacilities from '../../assets/icons/kitchen-facilities.svg';
import almundo from '../../assets/icons/logo-almundo.svg';
import newspaper from '../../assets/icons/newspaper.svg';
import nightclub from '../../assets/icons/nightclub.svg';
import restaurant from '../../assets/icons/restaurant.svg';
import safetyBox from '../../assets/icons/safety-box.svg';
import separateBredroom from '../../assets/icons/separate-bredroom.svg';
import sheets from '../../assets/icons/sheets.svg';
import search from '../../assets/icons/search.svg';

const icons = {
    [constants.icons.BEACH_POOL_FACILITIES]: beachPoolFacilities,
    [constants.icons.BUSINESS_CENTER]: businesCenter,
    [constants.icons.CHILDREN_CLUB]: childrenClub,
    [constants.icons.COFFE_MAKER]: coffeMaker,
    [constants.icons.DEEP_SOAKING_BATHTUB]: deepSoakingBathtub,
    [constants.icons.FITNESS_CENTER]: fitnessCenter,
    [constants.icons.KITCHEN_FACILITIES]: kitchenFacilities,
    [constants.icons.SAFETY_BOX]: safetyBox,
    [constants.icons.SEPARATE_BREDROOM]: separateBredroom,
    almundo,
    star,
    bathrobes,
    bathtub,
    beach,
    garden,
    newspaper,
    nightclub,
    restaurant,
    sheets,
    search
}

class Icon extends Component {
    render() {
        const {type} = this.props;
        return (
            <React.Fragment>
                {icons.hasOwnProperty(type) &&
                    <img src={icons[type]} alt="logo" />
                }
            </React.Fragment>
        );
    }
}

export { Icon };
