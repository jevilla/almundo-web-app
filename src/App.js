import React, { Component } from 'react';
import { Header, Card, Filter } from './components/';
import { strings } from './utils'
import './App.css';

import axios from 'axios';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hotels: []
        }
    }

    componentDidMount() {
        this.getAllHotels();
    }

    getHotelByName = (hotelName) => {
        axios({
            method: 'get',
            url: strings.ROOT_URL_SEARCH_HOTEL.concat(hotelName)
        }).then(hotel => {
            this.setState({ hotels: hotel.data });
        }).catch(err => console.error(err));
    }

    getAllHotels = () => {
        axios({
            method: 'get',
            url: strings.ROOT_URL_HOTELS
        }).then(hotel => {
            this.setState({ hotels: hotel.data });
        }).catch(err => console.error(err));
    }

    getHotelByName = (hotelName) => {
        axios({
            method: 'get',
            url: strings.ROOT_URL_SEARCH_HOTEL.concat(hotelName),
            headers: { Accept: 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        }).then((response) => {
            console.log(response.data);
            this.setState({ hotels: response.data })
        }).catch(err => console.error(err));
    }

    getHotelsByStars = (stars) => {
        axios({
            method: 'post',
            url: strings.ROOT_URL_SEARCH_HOTEL_BY_STARS,
            headers: { Accept: 'application/json' },
            data: { stars }
        }).then((response) => {
            console.log(response.data);
            this.setState({ hotels: response.data })
        }).catch(err => console.error(err));
    }

    hotelList = () => {
        const {hotels} = this.state;

        return hotels.map((hotel, index) => {
            return <Card key={hotel._id} {...hotel} />
        });
    }

    render() {
        return (
            <div className="App">
                <Header />
                <div id="main-container">
                    <div id="filter-container">
                        <Filter
                            getHotelByName={this.getHotelByName}
                            getAllHotels={this.getAllHotels}
                            getHotelsByStars={this.getHotelsByStars}
                        />
                    </div>
                    <div id="cardlist-container">
                        {this.hotelList()}
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
